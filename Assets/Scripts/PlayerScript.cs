﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {
	
	private float inputX = 0;
	private float inputY = 0;
	public Vector2 movement;
	private bool shoot = false;

	public Vector3 pos; 
	public  Vector3 dir;	
    public float toAngle;    

	private Stats stats; 
	private WeaponScript ws;
    private PhotonView view; 

	// Use this for initialization
	void Start () {
		stats = GetComponent<Stats> ();
		ws = GetComponent<WeaponScript>();
        view = GetComponent<PhotonView>();
	}
	
	// Update is called once per frame
	void Update () {
        
        if (view.isMine)
        {
            if (stats.isControllable)
            {
                shoot = Input.GetButton("Fire1"); //allow user to hold the button
                shoot |= Input.GetButton("Fire2");
                StartCoroutine(Shoot(shoot));
            }
        }
	}

    void Move(float inputX, float inputY)
    {        
        float x = 0;
        float y = 0;                
        rigidbody2D.MoveRotation(LookAtMouse()); 

        //This section handles adding force. inputX is a/d, inputY is w/s
        if (inputX > 0 && rigidbody2D.velocity.x < stats.maxSpeed)
        {
            x = (stats.speed.x * inputX);
            //rigidbody2D.MoveRotation(rigidbody2D.rotation + stats.rotationSpeed * Time.deltaTime); 
            //rigidbody2D.rotation -= stats.rotationSpeed;
        }
        if (inputX < 0 && rigidbody2D.velocity.x > -stats.maxSpeed)
        {
            x = (stats.speed.x * inputX);
            //rigidbody2D.MoveRotation(rigidbody2D.rotation - stats.rotationSpeed * Time.deltaTime);
            //rigidbody2D.rotation += stats.rotationSpeed;
        }
        if (inputY > 0 && rigidbody2D.velocity.y < stats.maxSpeed)
        {
            y = (stats.speed.y * inputY);                        
            //rigidbody2D.AddForce(transform.right * stats.maxSpeed);
        }
        if (inputY < 0 && rigidbody2D.velocity.y > -stats.maxSpeed)
        {
            y = (stats.speed.y * inputY);
            //rigidbody2D.AddForce(transform.right * -stats.maxSpeed);
        }        

        movement = new Vector2(x, y);
    }

	void FixedUpdate()
	{
        if (view.isMine)
        {
            if (stats.isControllable)
            {
                //These return -1, 0, or 1, so works well multiplied against speed.            
                inputX = Input.GetAxis("Horizontal");
                inputY = Input.GetAxis("Vertical");

                //Call the Move function to update the movement variable based on inputs
                //then this will be used for the rigidbody2d.velocity assignment to move the ship
                Move(inputX, inputY);
                rigidbody2D.WakeUp();
                rigidbody2D.AddForce(movement);
            }
            else
            {
                rigidbody2D.Sleep();
            }
        }
	}	

	public IEnumerator Shoot(bool shoot)
	{
		if (shoot)
		{       
			if (ws != null && ws.CanAttack && view.isMine)
			{
                Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
                Vector3 dir = Input.mousePosition - pos;
                float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
                Vector3 normal = dir.normalized;
                Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);                

                view.RPC("Attack", PhotonTargets.AllBufferedViaServer, 10f, normal, q);
				//ws.Attack(10f);
				//stats.isControllable = false; 
				yield return new WaitForSeconds(ws.shootingRate);
				//stats.isControllable = true; 
			}   
			else
			{				
			}
		}
	}
	
	public float LookAtMouse()
	{
		pos = Camera.main.WorldToScreenPoint(transform.position);
	    dir = Input.mousePosition - pos;
	    toAngle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        return toAngle;
		//transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
	}
}
