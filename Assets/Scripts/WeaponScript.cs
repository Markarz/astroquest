﻿using UnityEngine;
using System.Collections;

public class WeaponScript : MonoBehaviour {

	public Transform shotPrefab;
	public AudioClip shotAudio; 

	public bool CanAttack { get { return shootCooldown <= 0f; } }	
	public float shootCooldown { get; set; }
	public float shootingRate = 0.25f;
    private int team = -1;

	void Start () {
        team = transform.GetComponent<TeamMember>().teamID;
	}
	
	// Update is called once per frame
	void Update () {
		shootCooldown -= Time.deltaTime;		
	}

	[RPC]
	public void Attack(float damage, Vector3 pos, Quaternion rot)
	{                      
		if (CanAttack)
		{            
			//Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
			//Vector3 dir = Input.mousePosition - pos;
			//float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
			//Vector3 normal = dir.normalized;
			//Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
			Transform shotTransform = Instantiate(shotPrefab) as Transform;
			
			Shots ss = shotTransform.GetComponent<Shots>();
			ss.SetID(GetInstanceID());
			ss.damage = damage;
			
			shotTransform.position = transform.position;
			shotTransform.Rotate(rot.eulerAngles);

			shotTransform.GetComponent<TeamMember>().SetTeamID(team);           
			shotTransform.GetComponent<Shots>().SetID(GetInstanceID());                        
			
			Movement move = shotTransform.gameObject.GetComponent<Movement>();
			if (move != null)
			{
				//move.direction = this.transform.right; // towards in 2D space is the right of the sprite
				move.direction = pos;
			}
			
			audio.PlayOneShot(shotAudio);
			
			shootCooldown = shootingRate;
		}
		else
		{			
		}
	}



}
