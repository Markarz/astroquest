﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NetworkManager : MonoBehaviour {

    public GameObject standbyCamera;    
    public bool offlineMode = false;
    
    public List<string> chatMessages;
    int maxChatMessages = 10;

    public float respawnTimer = 0;
    public bool startTimer = false;

    public bool hasPickedTeam = false;    

    public bool testing = false;

    public string GameMode = "LivesRemaining";
    public int RedTeamLives = 50;
    public int GreenTeamLives = 50;

    public Transform playernameInput;
    public string playername;     

    // Use this for initialization
    void Start()
    {

        //PhotonNetwork.offlineMode = true;
        //PhotonNetwork.CreateRoom("Offline Room");
        //PhotonNetwork.logLevel = PhotonLogLevel.Full;        

        //PhotonNetwork.player.name = PlayerPrefs.GetString("Username", "Player");        
        playername = "Mark";         
        chatMessages = new List<string>();
        Connect();
    }

    //void Update()
    //{
    //    if (respawnTimer > 0 && startTimer)
    //    {
    //        respawnTimer -= Time.deltaTime;

    //        if (respawnTimer <= 0)
    //        {
    //            // Time to respawn the player!
    //            SpawnMyPlayer(teamID);
    //        }
    //    }
    //}
            
    public void Connect()
    {
        PhotonNetwork.ConnectUsingSettings("Astroquest v1");
        Debug.Log("Connecting");
    }

    public void OnJoinedLobby()
    {
        Debug.Log("Joined lobby");
        PhotonNetwork.JoinRandomRoom();
    }

    void OnPhotonRandomJoinFailed()
    {
        Debug.Log("Joining random game failed. Trying to create a room.");
        bool create = PhotonNetwork.CreateRoom(null);
        if (!create)
            Debug.Log("Failed to create a room. The network may be down.");
        else
            Debug.Log("Created a room.");
    }

    void OnJoinedRoom()
    {
        Debug.Log("Joined room. Spawning player");
        SpawnPlayer();
    }

    void OnDestroy()
    {
        PlayerPrefs.SetString("Username", PhotonNetwork.player.name);
    }

    public void SpawnPlayer()
    {                
        Vector3 randomStart = new Vector3(2, -2, -1);
        
        GameObject player = PhotonNetwork.Instantiate("Ship", randomStart, Quaternion.identity, 0);
        player.GetComponent<PhotonView>().RPC("SetTeamID", PhotonTargets.AllBuffered, 1);        
        PhotonNetwork.player.name = playername;

        Health health = player.GetComponent<Health>();
        health.enabled = true;
        
    }

    public void SpawnMyPlayer(int TeamID)
    {
        //teamID = TeamID;

        //if (spawnSpots == null)
        //{
        //    Debug.LogError("No spawn spots.");
        //    return;
        //}

        //SpawnSpot mySpawnSpot = spawnSpots[Random.Range(0, spawnSpots.Length)];
        //Vector3 spawnpos = new Vector3(mySpawnSpot.transform.position.x, mySpawnSpot.transform.position.y, -1);
        //GameObject player = PhotonNetwork.Instantiate(character, spawnpos, Quaternion.identity, 0);

        //PlayerScript playerscript = player.GetComponent<PlayerScript>();
        //playerscript.character = character;

        //Stats stats = player.GetComponent<Stats>();
        //stats.isControllable = true;

        //standbyCamera.SetActive(false);
        //GameObject cam = player.transform.FindChild("Main Camera").gameObject;
        //cam.SetActive(true);
        //cam.camera.enabled = true;
        //cam.GetComponent<AudioListener>().enabled = true;

        //GUIScript guiscript = player.GetComponent<GUIScript>();
        //guiscript.enabled = true;

        //player.GetComponent<PhotonView>().RPC("SetTeamID", PhotonTargets.AllBuffered, TeamID);
        //LowerTeamLives(TeamID);
        //startTimer = false;
        //DisplayAbilityHotkeys();
        //AddChatMessage(PhotonNetwork.player.name + " is respawning. Red lives: " + RedTeamLives + " Green lives: " + GreenTeamLives);
        ////disable the loading screen for performance
        //ls.enabled = false;
    }

    void LowerTeamLives(int TeamID)
    {
        if (TeamID == 1)
        {
            if (RedTeamLives - 1 > 0)
            {
                RedTeamLives -= 1;
            }
            else
            {
                //Start new match
                Debug.Log("Green team wins.");
            }
        }

        if (TeamID == 2)
        {
            if (GreenTeamLives - 1 > 0)
            {
                GreenTeamLives -= 1;
            }
            else
            {
                //Start new match
                Debug.Log("Red team wins.");
            }
        }


    }
    void OnGUI()
    {
        // We are fully connected, make sure to display the chat box.
        DisplayChatBox();
    }

    void DisplayChatBox()
    {

        GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
        GUILayout.BeginVertical();
        GUILayout.FlexibleSpace();

        foreach (string msg in chatMessages)
        {
            GUILayout.Label(msg);
        }

        GUILayout.EndVertical();
        GUILayout.EndArea();
    }
    public void AddChatMessageLocal(string m)
    {
        AddChatMessage_RPC(m);
    }
    public void AddChatMessage(string m)
    {
        GetComponent<PhotonView>().RPC("AddChatMessage_RPC", PhotonTargets.AllBuffered, m);
    }

    [RPC]
    void AddChatMessage_RPC(string m)
    {
        while (chatMessages.Count >= maxChatMessages)
        {
            chatMessages.RemoveAt(0);
        }
        chatMessages.Add(m);
    }
}
