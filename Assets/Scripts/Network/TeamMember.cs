﻿using UnityEngine;
using System.Collections;

public class TeamMember : MonoBehaviour
{

    public int _teamID = 3;
    public int teamID
    {
        get { return _teamID; }
    }

    [RPC]
    public void SetTeamID(int id)
    {
        _teamID = id;
    }
}
