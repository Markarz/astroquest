﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

	private Stats stats;
    private TeamMember team; 
	
	void Start () {
		stats = GetComponent<Stats>();
        team = GetComponent<TeamMember>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D otherCollider)
	{        
		Shots shot = otherCollider.gameObject.GetComponent<Shots>();
		PlayerScript ps = GetComponent<PlayerScript>();

        if (shot != null && ps != null)
		{
            var shotTeam = shot.GetComponent<TeamMember>().teamID;
            
            //Both colliders on same team, don't damage or destroy the shot.
            if (shotTeam != team.teamID)
            {
                float modifiedDamage = (shot.damage - stats.armor);

                if (modifiedDamage > 0)
                {
                    LowerHealth(modifiedDamage);
                }

                Destroy(shot.gameObject);
            }
            
		}
        
	}

	private void Die()
	{                		           
		Destroy(gameObject);
	}

    public void AddHealth(float amount)
    {
        if (amount > 0)
        {
            Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);

            if (stats.health + amount > stats.maxHealth)
            {
                stats.health = stats.maxHealth;
            }
            else
            {
                stats.health += amount;
            }
        }
    }
    public void LowerHealth(float amount)
    {        
        if (amount > 0)
        {
            Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);

            if (stats.health - amount <= 0)
            {
                stats.health = 0;
                Die();
            }
            else
            {
                stats.health -= amount;
            }
        }
    }

}
