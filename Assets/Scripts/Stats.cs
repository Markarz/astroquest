﻿using UnityEngine;
using System.Collections;

public class Stats : MonoBehaviour {

	public Vector2 speed = new Vector2(1f, 1f);
    public float maxSpeed = 1f;
    public Vector2 velocity;

    public float health = 10f;
    public float maxHealth = 10f; 
    public float armor = 0f; 

    public float rotationSpeed = 180; 
     
	public bool isControllable = true; 

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        velocity = rigidbody2D.velocity;
	}
}
