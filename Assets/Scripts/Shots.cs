﻿using UnityEngine;
using System.Collections;

public class Shots : MonoBehaviour {

    public float damage { get; set; } 
	public float id { get; set; }

	void Start () {
        damage = 1f; 
		Destroy (gameObject, 3f);
	}		

	[RPC]
	public void SetID(int shotid)
	{
		id = shotid;
	}


}
