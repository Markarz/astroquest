﻿using UnityEngine;
using System.Collections;

public class Walls : MonoBehaviour {

    public bool isDestructable = false;
	// Use this for initialization
	void Start () {
        InstantiateWalls();
	}

    //Walls must have IsTrigger set for this function to work
    void OnTriggerEnter2D(Collider2D otherCollider)
    {
        Debug.Log("wall collision");
        
        Shots shot = otherCollider.gameObject.GetComponent<Shots>();

        if(otherCollider.tag == "Player")
        {
            //Player collided with a wall
            Stats stats = otherCollider.GetComponent<Stats>();
            stats.velocity = Vector2.zero;
        }

        if (shot != null)
        {
            //Destroy shots for now.
            //TODO: Add things like reflective walls, etc. 
            Destroy(shot.gameObject);
        }        
        
    }

    private void InstantiateWalls()
    {
        foreach (Transform child in transform)
        {
            var childscript = child.gameObject.AddComponent<Walls>();                        
        }
    }
}
